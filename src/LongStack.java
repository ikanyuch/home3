import java.util.*;
import java.util.function.BiFunction;
import java.util.regex.Pattern;

import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class LongStack {

   private final static Map<String, BiFunction<Long, Long, Long>> operations;

   private Deque<Long> stack;

   //testid, vaatan mis lootakse õigselt. kui lootakse ei ole õige siis tuleb exseption
   public static void main(String[] argum) {
      LongStack ls = new LongStack();

      ls.push(11);
      ls.push(13);
      ls.op("-");
      System.out.println(ls.tos());
      assertEquals(ls.pop(), -2L);

      ls.push(12);
      ls.push(4);
      ls.op("/");
      System.out.println(ls.tos());
      assertEquals(ls.pop(), 3L);

      assertSout("4 4 -", 0l);
      assertSout("22 41 +", 63l);
      assertSout("21 7 /", 3l);
      assertSout("11 1 *", 11l);
      assertSout("12 2 * 4 - 4 /", 5l);
   }

   // see on koht kus mina alustan koodi tööda. tema prindib tulemus
   private static void assertSout(String pol, Long expected) {
      Long actual = LongStack.interpret(pol);
      System.out.println(format("%s = %d", pol, actual));
      assertEquals(expected, actual);
   }
   // kui ei ole võrdlust siis exseption
   private static void assertEquals(Long expected, Long actual) {
      if (!actual.equals(expected))
         throw new RuntimeException("not equals!");
   }
// muutuja on staatiline see tähendab et paneme tema siin
   static {
      operations = new HashMap<>();
      defineOperations();
   }
// viided meetodidesse millised on operatsioonide mörgid
   private static void defineOperations() {
      operations.put("+", Math::addExact);
      operations.put("-", Math::subtractExact);
      operations.put("*", Math::multiplyExact);
      operations.put("/", LongStack::divide);
   }
// kuna Math ei ole jagammis siis pean teha tema ise
   private static Long divide(Long long1, Long long2) {
      return long1 / long2;
   }
// see on konstruktor
   LongStack() {
      stack = new LinkedList<>();
   }

   // kopeerin uus objekt Stack listist
   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack copy = new LongStack();
      copy.stack = new LinkedList<>(stack);
      return copy;
   }
// testin kas Stack on tühi
   public boolean stEmpty() {
      return stack.isEmpty();
   }
// panen element Stekisse
   public void push(long a) {
      stack.push(a);
   }
// võtan element stekist, kui ta on tühi siis teen exception
   public long pop() {
      try {
         return stack.pop();
      } catch(NoSuchElementException e) {
         throw new RuntimeException("Not enough elements in stack!");
      }
   }
// teen  operatsioon opertsiooni märgi jargi argumentist. Võtame tema  ja kui tema ei ei ole siis exception
   public void op(String s) {
      BiFunction<Long, Long, Long> operation = operations.get(s);
      if(operation == null)
         throw new RuntimeException("Unknown symbol: " + s);

      // võtan kaks elementid operatsioonise, kui nende ei iisa tuleb exception
      long l2 = pop();
      long l1 = pop();
      // panen resultaat stekisse
      stack.push(operation.apply(l1, l2));
   }
// vaatan pealt poolne element aga ei võtan tema - hoian tema stekis
   public long tos() {
      Long element = stack.peek();
      if(element == null)
         throw new RuntimeException("Stack is empty");
      return element;
   }
// kas stekid on võrdlevad
   @Override
   public boolean equals(Object o) {
      if (o == null)
         return false;
      if (o == this)
         return true;
      if (!(o instanceof LongStack))
         return false;
      LongStack other = (LongStack) o;
      return stack.equals(other.stack);
   }
// tähtajade esitlus
   @Override
   public String toString() {
      LinkedList<Long> copy = new LinkedList<>(stack);
      Collections.reverse(copy);
      return copy.stream().map(String::valueOf).collect(joining(" "));
   }
// pea meetod, võtab tähtaja ja proovib tema arvutada
   public static long interpret(String pol) {
      LongStack ls = new LongStack();
      pol = pol.replaceAll("\t", " ").trim();
      // asendan taabid tühikule
      try {
         Arrays.stream(pol.split(" +"))
                 .forEach(str -> {
                    // vaatan kõik elemendis - kui see on number siis panen stekisse
                    if (isNumber(str))
                       ls.push(parseLong(str));
                    // kui ei ole teen operatsioon, mis võrde selle märki
                    else
                       ls.op(str);
                 });
// kui lõpuks on ei ole ainult üks element siis viga
         if (ls.stack.size() > 1)
            throw new RuntimeException("More than one number left in stack: " + ls.toString());
         // kui üldse ei ole elementid siis ka viga
         if (ls.stEmpty())
            // siin mina selline viga püüan
            throw new RuntimeException("Stack cannot be empty after all operations are done");

      } catch(RuntimeException e) {
         throw new RuntimeException("Input: " + pol + "; " + e.getMessage());
      }
      //resultaat
      return ls.pop();
   }
// vaatan kas see on number (võib olla see on miinuse märk ja ainult pärast on number)
   private static boolean isNumber(String s) {
      return Pattern.matches("-?[0-9]+", s);
   }

}